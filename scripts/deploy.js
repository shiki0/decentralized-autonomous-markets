// scripts/deploy_upgradeable_adminbox.js
const { ethers, upgrades } = require('hardhat');

async function main() {
	const DecentralizedAutonomousMarket_Escrow = await ethers.getContractFactory('DAM_Escrow');
	const DecentralizedAutonomousMarket_Token = await ethers.getContractFactory('DAM_NFT');
	
	console.log('Deploying DecentralizedAutonomousMarket...');
	const DAMe = await upgrades.deployProxy(DecentralizedAutonomousMarket_Escrow, 
	//address _paymentTokenAddress, address _owner, uint _mintingNewAssayerCost, uint16 _feePercent, uint _auditTimeExtension
	["0x7113B7838990aB90851e086d4a04558ab7e4528e", "0x133b83369d2EAdf51F659270f85C0a473629Be87",1000000000000000,100,5760], 
	{ initializer: 'initialize', kind: 'uups' });
	await DAMe.deployed();
	console.log('DecentralizedAutonomousMarket_Escrow deployed to:', DAMe.address);
	const DAMt = await upgrades.deployProxy(DecentralizedAutonomousMarket_Token, ["DecentralizedAutonomousMarket", "DAM", DAMe.address], 
	{ initializer: 'initialize', kind: 'uups' });
	await DAMt.deployed();
	console.log('DecentralizedAutonomousMarket_Token deployed to:', DAMt.address);
}

main()
    .then(() => process.exit(0))
    .catch(error => {
      console.error(error);
      process.exit(1);
    });
