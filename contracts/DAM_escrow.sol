// SPDX-License-Identifier: MIT
pragma solidity >=0.8.0 <0.9.0;
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";

interface IERC20 {
	function transfer(address _to, uint256 _amount) external returns (bool);
	function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);
}

interface IERC721 {
	function ownerOf(uint tokenId) external returns (address);
	function tokenId() external view returns (uint);
}

interface ECDSA {
	function valid(address signer, bytes memory hash, bytes memory signature) external view returns (bool);
}

contract DAM_Escrow is UUPSUpgradeable {
	
	struct marketData{
		address owner;
		address delegate; // address of delegate/minion
		address assayerNFT; //ERC721
		uint[] otherMarkets; // list of IDs of other markets - assayers
		uint16 feePercent;// = 100; //1%
		uint auditTimeExtension;// = 5760; // 1 day
	}

	struct transactionData{
		address buyer; 
		address seller;
		bytes32 spec; 
		address paymentTokenAddress; 
		uint paymentAmount; 
		uint expiry; //block number
		bytes buyerSignature;
		bytes sellerSignature;
		uint[] auditIDs; //list od audits - auditIDs
		bool finished; //placeholder for when buyer/seller withdraw funds or for partial order execution
		uint marketID;
	}

	struct assayerInput{
		address assayerNFT;
		uint tokenID; // NFT token ID with which assayer is participating - assayers can hold / and partiipate with multiple NFT tokens
		bytes32 commit;
		bool revealed;
		bool paid;
	}

	struct auditData{
		bytes32 buyerEvidence;
		bytes32 sellerEvidence;
		uint expiry; //block number
		uint price;
		address paymentTokenAddress; 
		bool resolved; // set true if delegate/minion decides
		bool refund; // direction of releasing funds - TRUE for buyer, FALSE for seller
		bool resolvedByVerdict; //resolved by unanimous decision of assayers
		assayerInput[] commits; 
		bool[] verdicts; // false in favor of seller, true in favor of buyer - refund
		uint marketID;
	}
	
	marketData[] markets;
	mapping(uint => mapping(uint => bool)) public banned; // marketID => assayerNFT# - true if banned
	mapping(uint => mapping(uint => uint)) openTransacion; //marketID => partial order transactionID => open order transactionID
	transactionData[] transactions;
	auditData[] audits;
	address ecdsa;
	
	event Transaction(uint transactionID);
	event Audit(uint auditID);
	event CommittedAssay(address indexed assayer, uint auditID, uint assayID);
	event RevealedAssay(address indexed assayer, uint auditID, uint assayID, bool refund);
	event NewMarket(uint marketID);
	event OtherMarketID(uint marketID, uint otherMarketID); 
	event OpenTransaction(uint transactionID);

	function initialize(address _owner, address _assayerNFT, uint16 _feePercent, uint _auditTimeExtension, address _ecdsa) initializer public {
		__UUPSUpgradeable_init();
		createMarket(_owner, _assayerNFT, _feePercent, _auditTimeExtension);
		ecdsa=_ecdsa;
	}
	
	function _authorizeUpgrade(address newImplementation) internal override onlyOwner(0) {}
	
	function createMarket(address _owner, address _assayerNFT, uint16 _feePercent, uint _auditTimeExtension) public {
		emit NewMarket(markets.length);
		markets.push();
		markets[markets.length-1].owner=_owner; //multisig contract / gnosis safe
		markets[markets.length-1].assayerNFT=_assayerNFT;
		if(_feePercent<=10000)markets[markets.length-1].feePercent=_feePercent;
		markets[markets.length-1].auditTimeExtension=_auditTimeExtension;
	}
	
	function takeOpenTransaction(uint transactionID, bytes memory receipt, bytes32 spec, uint offerAmount, uint counterAmount) external {
		require(transactions[transactionID].seller == address(0) && !transactions[transactionID].finished , "26");
		if(counterAmount > 0) { // joining transaction with own funds - can be premeditated as the buyer already defined the 'counterAmount' in his initial buyerSignature - or just the same as paymentAmounth 
			//this special case does not require confirmation from buyer
			require(ECDSA(ecdsa).valid(transactions[transactionID].buyer,abi.encodePacked(counterAmount),transactions[transactionID].buyerSignature), "3");
			require(IERC20(transactions[transactionID].paymentTokenAddress).transferFrom(msg.sender, address(this), counterAmount), "1"); 
			transactions[transactionID].paymentAmount+=counterAmount;
			transactions[transactionID].seller=msg.sender;
			transactions[transactionID].expiry+=block.number;
			delete transactions[transactionID].sellerSignature; //blanking sellerSignature - later used in special withdraw case
			emit Transaction(transactionID);
		}
		else {
			transactionData memory temp;
			temp=transactions[transactionID];
			temp.seller=msg.sender;
			temp.spec=spec;
			temp.paymentAmount=offerAmount;//if offerAmounth is lower then the paymentAmount then it will be treated as partial transaction
			temp.finished = true; // locking the transaction untill, buyer confirms
			temp.sellerSignature = receipt;
			//emit Transaction(transactions.length);
			emit OpenTransaction(transactions.length);
			openTransacion[transactions[transactionID].marketID][transactions.length]=transactionID;
			transactions.push(temp);	
		}
	}
	
	function confirmTransaction(uint transactionID) external {
		require(msg.sender==transactions[ openTransacion[transactions[transactionID].marketID][transactionID] ].buyer, "13");//only buyers that created "open transactions" can confirm resulting transactions
		if(transactions[ openTransacion[transactions[transactionID].marketID][transactionID] ].paymentAmount > transactions[transactionID].paymentAmount) {//confirming a partial order
			transactions[ openTransacion[transactions[transactionID].marketID][transactionID] ].paymentAmount -= transactions[transactionID].paymentAmount;
		}
		else {
			require(transactions[ openTransacion[transactions[transactionID].marketID][transactionID] ].finished == false , "5");
			transactions[ openTransacion[transactions[transactionID].marketID][transactionID] ].finished = true;
		}
		transactions[transactionID].finished = false;
		transactions[transactionID].expiry+=block.number;
		emit Transaction(transactionID);
	}

	function createTransaction(address buyer, address seller, bytes32 spec, address _paymentTokenAddress, uint paymentAmount, uint blocks, bytes memory  buyerSignature, bytes memory  sellerSignature, uint marketID) public {
		require(IERC20(_paymentTokenAddress).transferFrom(buyer, address(this), paymentAmount), "1"); 
		
		transactionData memory temp;
		
		if(seller != address(0)) { // messageHash = keccak256(abi.encodePacked( SPEC, PAYMENT TOKEN ADDRESS, PAYMENT AMOUNTH, BLOCKS - TIME )
			require(ECDSA(ecdsa).valid(seller,abi.encodePacked(spec, _paymentTokenAddress, paymentAmount, blocks),sellerSignature), "2");
			if(buyer != markets[marketID].owner) require(ECDSA(ecdsa).valid(buyer,abi.encodePacked(spec, _paymentTokenAddress, paymentAmount, blocks),buyerSignature), "3");
			temp.expiry = block.number + blocks;
		}
		else temp.expiry = blocks;
		
		temp.buyer = buyer;
		temp.seller = seller;
		temp.spec = spec;
		temp.paymentTokenAddress = _paymentTokenAddress;
		temp.paymentAmount = paymentAmount;
		temp.buyerSignature = buyerSignature;
		temp.sellerSignature = sellerSignature;
		temp.finished = false;
		emit Transaction(transactions.length);
		transactions.push(temp);
	}

	function swapAddress(uint transactionID, address newAddress) external {
		if(msg.sender==transactions[transactionID].buyer)transactions[transactionID].buyer=newAddress;
		if(msg.sender==transactions[transactionID].seller)transactions[transactionID].seller=newAddress;
	}

	function payWithFee(uint transactionID) internal {
		if(transactions[transactionID].buyer==markets[transactions[transactionID].marketID].owner || transactions[transactionID].seller==address(0)) //without fee if buyer is market owner or transaction getting canceled
			IERC20(transactions[transactionID].paymentTokenAddress).transfer(msg.sender, transactions[transactionID].paymentAmount);
		else {
			IERC20(transactions[transactionID].paymentTokenAddress).transfer(msg.sender, transactions[transactionID].paymentAmount - transactions[transactionID].paymentAmount/10000*markets[transactions[transactionID].marketID].feePercent);
			IERC20(transactions[transactionID].paymentTokenAddress).transfer(markets[transactions[transactionID].marketID].owner, transactions[transactionID].paymentAmount/10000*markets[transactions[transactionID].marketID].feePercent);
		}
		transactions[transactionID].finished = true;
	}

	function withdraw(uint transactionID) external {
		require(transactions[transactionID].expiry < block.number, "4");
		require(!transactions[transactionID].finished, "5");
		if (msg.sender == transactions[transactionID].seller) { //checking if withdraw was called by seller
			if (transactions[transactionID].auditIDs.length > 0) { //checking if there were audits
				uint resolved;//checking if all audits were resolved and in favor of seller
				for (uint i; i < transactions[transactionID].auditIDs.length; i++) { // first we check if audit was ruled by delegate / minion
					if (audits[transactions[transactionID].auditIDs[i]].resolved && !audits[transactions[transactionID].auditIDs[i]].refund) 
						resolved++;
					else { //then we check if all assayers voted in the favor of the seller
						require(audits[transactions[transactionID].auditIDs[i]].expiry < block.number, "6");
						require(audits[transactions[transactionID].auditIDs[i]].commits.length == audits[transactions[transactionID].auditIDs[i]].verdicts.length , "7");
						uint verdicts;
						for(uint j; j < audits[transactions[transactionID].auditIDs[i]].verdicts.length; j++){
							if (!audits[transactions[transactionID].auditIDs[i]].verdicts[j])
								verdicts++;
						}
						if (verdicts != 0 && verdicts == audits[transactions[transactionID].auditIDs[i]].verdicts.length){
							resolved++;
							audits[transactions[transactionID].auditIDs[i]].resolvedByVerdict = true;
						}
					}
				}
				require(resolved == transactions[transactionID].auditIDs.length, "8");
			}
            payWithFee(transactionID);
		}
		else if (msg.sender == transactions[transactionID].buyer) {// checking if withdraw was called by buyer
			if(transactions[transactionID].seller!=address(0)){ // allowing withdraw if sellers address is null
				uint resolved;//checking if all audits were resolved and in favor of buyer
				if (transactions[transactionID].auditIDs.length > 0) // checking if there were any audits on transaction
					for (uint i; i < transactions[transactionID].auditIDs.length; i++) { // first we check if audit was ruled by delegate / minion
						if (audits[transactions[transactionID].auditIDs[i]].resolved && audits[transactions[transactionID].auditIDs[i]].refund) 
							resolved++;
						else { //then we check if all assayers voted in the favor of the buyer
							require(audits[transactions[transactionID].auditIDs[i]].expiry < block.number, "6");
							require(audits[transactions[transactionID].auditIDs[i]].commits.length == audits[transactions[transactionID].auditIDs[i]].verdicts.length , "7");
							uint verdicts;
							for(uint j; j < audits[transactions[transactionID].auditIDs[i]].verdicts.length; j++){
								if (audits[transactions[transactionID].auditIDs[i]].verdicts[j])
									verdicts++;
							}
							if (verdicts != 0 && verdicts == audits[transactions[transactionID].auditIDs[i]].verdicts.length) {
								resolved++;
								audits[transactions[transactionID].auditIDs[i]].resolvedByVerdict = true;
							}
						}
					}
				else require(transactions[transactionID].sellerSignature.length == 0); // allowing withdraw for cases where both parties added founds
				require(resolved == transactions[transactionID].auditIDs.length, "8"); // allowing withdraw where audits went in the favor of buyer
			}
			payWithFee(transactionID);
		}
	}

	modifier auditNotExpired(uint _auditID) {
		require(audits[_auditID].expiry > block.number, "10");
		_;
	}

	function audit(uint transactionID, uint blocks,address paymentTokenAddress, uint auditPrice) 
	external {
		require(IERC20(paymentTokenAddress).transferFrom(msg.sender, address(this),auditPrice), "1"); 
		require(transactions[transactionID].expiry > block.number, "9");
		transactions[transactionID].auditIDs.push(audits.length);
		emit Audit(audits.length);
		audits.push();
		audits[audits.length-1].expiry = block.number + blocks;
		audits[audits.length-1].paymentTokenAddress = paymentTokenAddress;
		audits[audits.length-1].price = auditPrice;
		audits[audits.length-1].marketID = transactions[transactionID].marketID;
	}

	function fillEvidence(uint transactionID, uint _auditID, bytes32 evidence) 
	auditNotExpired(_auditID)
	external {
		if(transactions[transactionID].buyer == msg.sender) audits[_auditID].buyerEvidence = evidence;
		else if(transactions[transactionID].seller == msg.sender) audits[_auditID].sellerEvidence = evidence;
		else revert("13");
	}
	
	function resolveAudit(uint _auditID, bool refund) external onlyOwner(audits[_auditID].marketID) {
		require(!audits[_auditID].resolvedByVerdict && !audits[_auditID].resolved, "15");
		audits[_auditID].resolved = true;
		audits[_auditID].refund = refund;
		IERC20(audits[_auditID].paymentTokenAddress).transfer(msg.sender, audits[_auditID].price);
	}

	function commitAssay(uint _auditID, bytes32 saltedCommit, address assayerNFT, uint tokenID) 
	auditNotExpired(_auditID)
	external {
		require(IERC721(assayerNFT).ownerOf(tokenID)==msg.sender, "25"); // confirm assayerNFT ownership
		if(assayerNFT == markets[audits[_auditID].marketID].assayerNFT) require( !banned[audits[_auditID].marketID][tokenID] ,"16"); // assayer with "default" assayerNFT - must not be banned
		else { // assayer with otherMarkets assayerNFT
			bool otherMarketNFT; 
			uint otherMarketID;
			for(;otherMarketID < markets[audits[_auditID].marketID].otherMarkets.length; otherMarketID++)
				if(markets[ markets[audits[_auditID].marketID].otherMarkets[otherMarketID] ].assayerNFT == assayerNFT) {
					otherMarketNFT = true;
					break;
				}
			require(otherMarketNFT && !banned[ markets[audits[_auditID].marketID].otherMarkets[otherMarketID] ][tokenID], "17");
		}
		for(uint i; i < audits[_auditID].commits.length; i++){
			require(!(audits[_auditID].commits[i].assayerNFT == assayerNFT && audits[_auditID].commits[i].tokenID == tokenID),"18");
		}
		emit CommittedAssay(msg.sender, _auditID, audits[_auditID].commits.length);
		audits[_auditID].commits.push(assayerInput(assayerNFT, tokenID, saltedCommit, false, false));
	}
	
	modifier trueAssayer(uint _auditID, uint assayID) {//verifying if caller is the true owner of assay in question
		require(IERC721( audits[_auditID].commits[assayID].assayerNFT ).ownerOf( audits[_auditID].commits[assayID].tokenID )==msg.sender, "19");
		_;
	}

	function revealAssay(uint _auditID, bool refund, bytes32 salt, uint assayID) 
	trueAssayer(_auditID, assayID)
	auditNotExpired(_auditID)
	external {
		require(!audits[_auditID].commits[assayID].revealed, "21");
		require(getSaltedHash(refund,salt)==audits[_auditID].commits[assayID].commit,"22");
		audits[_auditID].commits[assayID].revealed=true;
		audits[_auditID].verdicts.push(refund);
		emit RevealedAssay(msg.sender, _auditID, assayID, refund);
		if(audits[_auditID].expiry < (block.number + markets[audits[_auditID].marketID].auditTimeExtension))
			audits[_auditID].expiry = block.number + markets[audits[_auditID].marketID].auditTimeExtension;
	}

	function assayerWithdrawFee(uint _auditID, uint assayID)
	trueAssayer(_auditID, assayID)
	public {
		require(audits[_auditID].resolvedByVerdict, "23");
		require(!audits[_auditID].commits[assayID].paid, "24");
		IERC20(audits[_auditID].paymentTokenAddress).transfer(msg.sender, audits[_auditID].price / audits[_auditID].verdicts.length);
		audits[_auditID].commits[assayID].paid = true;
	}

	function assayerWithdrawFees(uint[2][] memory assays) external {
		for(uint i; i<assays.length; i++)
			assayerWithdrawFee(assays[i][0], assays[i][1]);
	}
	
	function getSaltedHash(bool refund,bytes32 salt) public view returns(bytes32){
		return keccak256(abi.encodePacked(address(this), refund, salt));
	}

	modifier onlyOwner(uint marketID) {
		require(msg.sender == markets[marketID].owner, "25");
		_;
	}
	modifier onlyOwnerOrDelegate(uint marketID) {
		require(msg.sender == markets[marketID].owner || msg.sender == markets[marketID].delegate , "25");
		_;
	}
	function transferOwnership(address newowner, uint marketID) external onlyOwner(marketID) {
		markets[marketID].owner = newowner;
	}

	function updateOtherMarkets(uint[] memory otherMarkets, uint marketID) external onlyOwner(marketID){
		markets[marketID].otherMarkets = otherMarkets;
		for(uint i; i<otherMarkets.length; i++)
		emit OtherMarketID(marketID, otherMarkets[i]);
	}

	function updateMarket(address _delegate, uint _auditTimeExtension, uint16 _feePercent, address _assayerNFT, uint marketID) external onlyOwner(marketID){
		markets[marketID].delegate=_delegate;
		markets[marketID].auditTimeExtension = _auditTimeExtension;
		if(_feePercent<=10000)markets[marketID].feePercent = _feePercent;
		markets[marketID].assayerNFT=_assayerNFT;
	}
	
	function banTokenID(uint[] memory tokenBanList, uint marketID) external onlyOwnerOrDelegate(marketID){
		for(uint i; i<tokenBanList.length; i++)
			banned[marketID][tokenBanList[i]]=true;
	}
	
	function getMarket(uint marketID) external view returns(marketData memory) {
        return markets[marketID];
    }
	
	function getTransaction( uint transcationID) external view returns(transactionData memory) {
        return transactions[transcationID];
    }
	
	function getAudit(uint auditID) external view returns(auditData memory) {
        return audits[auditID];
    }
}