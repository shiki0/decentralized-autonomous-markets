// SPDX-License-Identifier: MIT
pragma solidity >=0.8.0 <0.9.0;

import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import "@openzeppelin/contracts/utils/cryptography/SignatureChecker.sol";


contract ECDSA_min{
    function valid(address signer, bytes memory hash, bytes memory signature) public view returns (bool) {
        require(SignatureChecker.isValidSignatureNow(signer, ECDSA.toEthSignedMessageHash(keccak256(hash)), signature), "Hashed signature not valid.");
        return true;
    }
}