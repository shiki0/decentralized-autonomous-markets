# Decentralized Autonomous Markets


A design pattern for open, comunity driven decentralized marketplaces. Structured as a open scientific colaboration centered on a commonly agreed quantifiable objective, ie maximize total welfare estimated in such and such a way. A series of retroactive estimates of the value of contributions.

The key economic tradeoff of a marketplace that is mediated by open source computational contracts is how to fund the development of those contracts. The welfarimizing policy is to charge a transaction fee that funds improvements to the contracts when the gains in future welfare enough to balance the deadweight loss of welfare form the reduced trade due to the fee. When no further improvements can be made the welfare maximizing transaction fee is 0. 


We propose to allow permisionless (but bonded) proposals to modify the contracts. To evaluate payments for such proposals where possible using pre-registered and comunaly curated experimental protocols. Where experimental evaluation is not feasable, the protocol seek to still use pre-registered evaluation methodologies, and participation in these has as a pre-requisite continued ability to correctly predict outcomes in related experiments which can be experimentally evaluated.

Grant: https://gitcoin.co/grants/5130/decentralised-autonomous-markets \
Discord: https://discord.gg/2vMXGSAArj \
\
UI: https://gitlab.com/shiki0/dam-ui \
\
Escrow contract live on Optimism: https://optimistic.etherscan.io/address/0x06c3cb1da9e307124af2484974fa39f93703c282#code \
UI live on OP: https://dam.albin.si \
Escrow contract live on Goerli: https://goerli.etherscan.io/address/0x561F1B055128fF802fd983B17897911894c15FA0#code \
UI live on Goerli: https://dam.albin.si/goerli

## 




##  Permisionless impact evaluation not just proposal-aproval

For interventions that do not require the contracts to be changed, a bonded (to avoid vaocus placebo submissions that leech the false positive rate)  request for evaluation, which triggers a randomized onset 

## A use case: Open Buy Orders for arbitrary actions that retroactively can be shown to raise welfare


## Quality Assayers face lower costs of public good creation

thus subsidizing public goods also makes assayers have more demand for their knowledge. 


## Production of Knowledge Commons, The Iron Law of Oligarchy

Bootstrapping two sided markets involves solving two product market fit problems (one for buyers and one for sellers) simultanouesly. Platforms that manage this are then in a defensible position to extract substantial rents. 

Some informational public goods have markets that are highly complenentary. For example:

- the body of knowledge and practice that enables the grading of flour into standards, is complementary to the market for trading flour future contracts. 
- the open source audited smart contract libraries are complementary to the market for smart contract development services.
- standard, open, seed investment legal documents are complementary to the market for seed investmenting. 
- forensic accountancy techniques for fraud detection that can be scaled are complementary to most markets where there is room for fraud.


User ownership is not a panacea. If ownership is distributed based on marginal contribution a oligarchy of early users (or those who buy them out early). Proof of humanity is unlikley to solve this; is used to attempt a democratic form of governance  as the ocmplexity of the underlyign deicsions increases the standard tragedy of the commons of freeridding the cognitive contribution https://en.wikipedia.org/wiki/Iron_law_of_oligarchy

Two design criteria are:

1) Objective guided: what the market is trying to do is objectively quantifiable in an agreed upon manner. 
2) Permissionless: anyone can participate or propose changes to the structure. They are incorporated if they are expected to improve the objectives of the organization, irrespective of the identity or control of the proposer.  The marginal value of contributions is estimated ex-post and revenue streams of the market are divided proportional to these.

For example, the obective could be to maximize the total net present value of protocol fees that go to public goods that complement the protocol.

## Experiments in Algorithmic Retroactive Rewards


some tasks such as detecting malicious actors are valuable t the comunity, and much like code contribution




### Upgrade mechanism

each head of the source tree is a potential action in each epoch, we do thompson sampling based of the probability estimates from a market for contigent payouts off the kpi 


## Algorithmically

In an escrowed transaction between buyer and seller the release of escrow can be made dependent on the decision of one or more "assayers". Status as an assayer is demonstrated through control of an NFT, which can be burned by the DAO if the assayer is observed to act maliciously.


 
![](https://i.imgur.com/KDDgLb9.png)



## Contrast to existing commonly applied mechanisms:

In contrast to the standard model of distributing tokens based on use and allowing them some degree of governance via voting,  we instead seek to agree on the objective of the marketplace in terms of a measurable quantitty over transactions. Then anyone 


## Bootstrapping: the DAM DAM and initial buy orders




##  bootstrapping Schelling points in other verticals

conteinerized sheltered spaces; 20 foot side opening containers with a insulated box inside that can be rigged to the top, 4 

dna sequencing lab; nanopore and a protocol to get long dna, circulomics. very long very pure dna molecules



# Example applications

bug bounty via proof of exploit and proprotional payouts for making them inviable on main before exploited

solar powered water harvesting from air
https://academic.oup.com/ijlct/article/15/2/253/5718410



# Older


## Premisionless, User-Owned Markets


Potential changes to the market structures are at the discretion of market operators. Permissionless markets are markets where anyone may propose a change to the market structure, and there exists a computationally pre-specified process for evaluating each proposed change which ultimately relies on randomized experimentation to anchor the mechanism. Not all proposed market structure changes can be cleanly randomized, and even for those that can be the experiment cost can be high enough ex-ante to be avoided. We use randomization when feasible. When not we elicit counterfactual estimates.

Two sided online markets are usually bootstrapped by subsidizing supply (sean; ref to playbook more generally?). This subsidy comes from the sale of equity in the business. User Owned Markets disintermediate this, providing the ownership directly to participants proportionally to the value that they are adding to the network (todo: how to estimate the marginal value of a given unit of supply or demand?)

producers are users

## Anchoring the contracts and interfaces

each instance of a market has: 
- units for sale (points in version space of the public good spec and the quality/price market expectation of the provider at that price). For MVP these are off-chain, and we assumed  they are just text on ipfs that specifies things. 
- units of demand (mutatis mutandis)
- transactions: buyer id, seller id, point of agreement, price, delivery terms, public good contribution (aka fee)
- dispute resolution a la reality.eth with final judment by assay when possible and b y counterfactual panel when not. 
- assay: 
- counterfactual panel:  high quality assay, high intra rates validity, high value to bootstraping / early liquidity


###  MVP  interfaces

- market contract: receives transactions, holds escrow, provides dispute resolution
    - Allows anyone to send in new designs
    - When buyer and seller agree on version and price, a transaction is made where a buyer sends payment to the contract, where it is held in escrow til the buyer receives the product from the seller. Case 1: If the buyer does not dispute the transaction, the payment is removed from escrow and sent to the seller. Case 2: If the buyer does dispute the transaction, the commitee audit is called in and the decision goes to the assayer to handle the dispute, for the time being reality.eth will handle the dispute, with the final say being a multisig wallet.
    - When a transaction is made there is some fee, we try to divide this revenue stream proportional to the value the different 

## related materials

https://reality.eth.link/

https://www.smartcontractresearch.org/t/research-summary-schelling-games-and-p-epsilon-attacks/62

## incoherent ramblings
Consider a "market builder function" that given a specific vertical (say shelter, or water, or open source user-owned marketplace software), would instantiate a market.

By a market we mean: software implementation of demand and supply matching engine, provision of sufficient liquidity in the demand and supply that first enters the marketplace (potentially disintermediating VCs by having early users directly take control of the networks they helped bootstrap).

User  is constructed broadly: consumers, producers, assessors, contributors to the software, those running it,  that instantiates the markets.

For the concrete verticals given a metric that the market is optimizing that can be measured in some finite amount of time, some mixture of quality oracles (with inter-rater variability exercises to calibrate) and a expert committee for things directly unobservable, selected from the highest performing oracles for those observables. 

reality.eth exponential betting before delegating to oracles with rpobabiltiy increasing in the committed "tax" to the marketplace of the betting transaction.
 
 
The "assayers" for unobservable facts can be picked between those that are widely held as natural for the role in some schelling coordination game and those who have displayed particular skill as oracles for future assays of that which is observable. 

Starting with the marketplace for user owned marketplaces, instead of the marketplace for solidity devs or blockchain devs more broadly; allow us bootstrap potential distribution channel (entrepreneurs who start instances and find success) in a way that is more complementary to us that gneeric devs, since here we also use the open source code they are building/extending. In aprticular can have much more sophisticated measures of outcome quality than in a generic 
marketplace.


 user-owned institutions beyond markets; there is some market component somewhere to keep the lights on (eg even couchsurfing)
 
 
 complementarity
 
 
 crypto governance is broken; locking tokens for voting power has no acountability for wether the votes are constructive or not. Ultimately this requires evaluating the coutnerfactual outcome of decisions not taken, to align rewards. By building randomization into the market structure this counterfactual can be evaluated. Alternatively, for non-randomizable decisions 
 
 
 
![](https://imgs.xkcd.com/comics/dependency.png)








## Constructive Economics

tthe strategy space is effectively turing complete


